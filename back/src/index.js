// back/src/index.js
// import app from './app'
const app = require("./app");
const db = require("./db");

  const start = async () => {
    const controller = await db.initializeDatabase();
    app.get('/', (req, res) => res.send("ok"));
    app.get('/contacts', async (req, res) => {
      const contacts = await controller.getContactsList()
      res.send(contacts)
    })
  }  

  start();
  
app.listen(8000, () => console.log("server listening on port 8000"));