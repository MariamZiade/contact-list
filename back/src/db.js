// back/src/db.js
const sqlite = require("sqlite");
const sqlite3 = require("sqlite3");
const SQL = require("sql-template-strings");

const test = async () => {
  /**
   * create and connect to the db
   */
  const db = await sqlite.open({
    filename: "db.sqlite",
    driver: sqlite3.Database,
  });

  /**
   * Create the table
   **/
  await db.run(
    `CREATE TABLE contacts (contact_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email text NOT NULL UNIQUE);`
  );

  /**
   * let's insert a bit of data in it. We're going to insert 10 users
   * We first create a "statement"
   **/

  const stmt = await db.prepare(
    `INSERT INTO contacts (name, email) VALUES (?, ?)`
  );
  let i = 0;

  while (i < 10) {
    await stmt.run(`person ${i}`, `person${i}@server.com`);
    i++;
  }

  /** finally, we close the statement **/
  await stmt.finalize();

  /**
   * Then, let's read this data and display it to make sure everything works
   **/
  const rows = await db.all(
    "SELECT contact_id AS id, name, email FROM contacts"
  );
  rows.forEach(({ id, name, email }) =>
    console.log(`[id:${id}] - ${name} - ${email}`)
  );
};

const initializeDatabase = async () => {
  const db = await sqlite.open({
    filename: "db.sqlite",
    driver: sqlite3.Database,
  });

  /**
   * retrieves the contacts from the database
   */
  const getContactsList = async () => {
    const rows = await db.all("SELECT contact_id AS id, name, email FROM contacts");
    return rows;
  }
  const controller = {
    getContactsList
  }

  return controller;
}

const db = { initializeDatabase }

module.exports = db;