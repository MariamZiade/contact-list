import { Component } from 'react';
import logo from "./assets/images/logo.svg";
import "./App.css";
import Timer from './components/timer/Timer';


class App extends Component {

  state = {
    contacts: []
  }

  async componentDidMount() {
    try {
      const response = await fetch("http://localhost:8000/contacts");
      const contacts = await response.json();
      this.setState({ contacts })
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    let { contacts } = this.state;
    return (
      <div className="App">
        {/* <Timer timer = {1}/> */}
        { contacts.map(contact => (
          <div key={contact.id}>
            <p>{contact.id} -  {contact.name}</p>
          </div>
        ))}
      </div>
    )
  }
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

export default App;
