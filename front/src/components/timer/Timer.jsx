import { Component } from 'react';

export default class Timer extends Component {

    constructor(props) {
        super(props);
        let timer = this.props.timer;
        this.state = {
            timer
        }
    }

    // state = {
    //     timer: this.props.timer
    // }

    // componentWillMount() {}

    componentDidMount() {
        console.log("It is mounted")
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     this.props.timer != nextProps.timer;
    // }

    // componentWillUnmount() { }

    handleClick = () => {
        this.setState({ timer: this.state.timer + 1 })
    }

    render() {
        let { timer } = this.state;
        return (
            <div>
                <p>my timer: {timer} </p>
                <button onClick={this.handleClick}> Click Me </button>
            </div>
        )
    }
}
